# README #

This project has been written in Swift for iOS14.


### Included Features ###

* iPhone/iPad support
* Light/Dark mode support (app is permanently set to be dark mode display)
* API calls to provided endpoints
* Unit testing

### Notes ###

* There is a constraint conflict on the list of transactions.  For a production app I would not have left this, but for the purpose of this excercise I did not investigate further as the main intention was to provide the completed code for review.
* I would not normally have hard-coded links within the code as I have done for this project, but usually prefer to have a dedicated section for these.  Again, for the purpose of this excercise (and because there were so few endpoints) I have simply included them in code for time sake. 

