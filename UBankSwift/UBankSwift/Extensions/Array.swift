//
//  Array.swift
//  UBankSwift
//
//  Created by Shane Henderson on 6/5/21.
//

import UIKit

extension Array where Element: Datable {
    
    func grouped(by key: KeyPath<Element, Date>, order: ComparisonResult? = .orderedDescending) -> [Date: [Element]] {
        let initial: [Date: [Element]] = [:]
        let groupedByDateComponents = reduce(into: initial) { acc, cur in
            let components = Calendar.current.dateComponents([.year, .month, .day], from: cur[keyPath: key])
            let date = Calendar.current.date(from: components)!
            var existing = acc[date] ?? []
            existing += [cur]
            existing.sort { $0.date.compare($1.date) == order }
            acc[date] = existing
        }

        return groupedByDateComponents
      }
}

