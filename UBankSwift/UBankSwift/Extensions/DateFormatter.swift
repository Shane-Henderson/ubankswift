//
//  DateFormatter.swift
//  UBankSwift
//
//  Created by Shane Henderson on 6/5/21.
//

import UIKit

extension DateFormatter {
    
    static let apiFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        formatter.locale = Locale(identifier: "en_US_POSIX")

        return formatter
    }()
    
    static let displayFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEE MMM d, yyyy"
        formatter.locale = Locale(identifier: "en_US_POSIX")

        return formatter
    }()
}
