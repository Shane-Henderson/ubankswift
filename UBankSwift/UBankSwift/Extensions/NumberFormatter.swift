//
//  NumberFormatter.swift
//  UBankSwift
//
//  Created by Shane Henderson on 6/5/21.
//

import UIKit

extension NumberFormatter {
    
    static let currencyFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.currencySymbol = "$"
        formatter.locale = Locale(identifier: "en_AU")
        formatter.numberStyle = .currency
        formatter.usesGroupingSeparator = true
        
        return formatter
    }()
}
