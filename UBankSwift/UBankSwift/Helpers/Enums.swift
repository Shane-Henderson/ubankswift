//
//  Enums.swift
//  UBankSwift
//
//  Created by Shane Henderson on 6/5/21.
//

import UIKit

enum UBankError: Error {
    case dataError
    case decodingError
    case serverError
}
