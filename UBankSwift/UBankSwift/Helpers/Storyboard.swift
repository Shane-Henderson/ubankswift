//
//  Storyboard.swift
//  UBankSwift
//
//  Created by Shane Henderson on 6/5/21.
//

import UIKit

struct Storyboard {
    
    // MARK: - CELL IDENTIFIERS
    
    static let dateCellIdentifier = "DateViewCell"
    static let transactionCellIdentifier = "TransactionViewCell"
    
    
    // MARK: - VIEW CONTROLLERS
    
    static func accountViewController(_ account: Account) -> AccountViewController {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "AccountViewController") as! AccountViewController
        viewController.account = account

        return viewController
    }
    
}
