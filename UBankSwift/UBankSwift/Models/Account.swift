//
//  Account.swift
//  UBankSwift
//
//  Created by Shane Henderson on 6/5/21.
//

import UIKit

struct Account {

    // MARK: - PROPERTIES
    
    let id: String
    let availableBalance: Double
    let currentBalance: Double
    let productName: String
    
    
    // MARK: - INITIALISATION
    
    init?(_ response: AccountResponse) {
        guard let availableBalance = Double(response.availableBalance),
              let currentBalance = Double(response.currentBalance) else {
            return nil
        }
        
        id = response.id
        self.availableBalance = availableBalance
        self.currentBalance = currentBalance
        productName = response.productName
    }
}

// MARK: - HASHABLE

extension Account: Hashable {
    
    func hash(into hasher: inout Hasher) {
      hasher.combine(id)
    }

    static func == (lhs: Account, rhs: Account) -> Bool {
      lhs.id == rhs.id
    }
}
