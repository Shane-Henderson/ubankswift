//
//  AccountResponse.swift
//  UBankSwift
//
//  Created by Shane Henderson on 6/5/21.
//

import UIKit


struct AccountResponse: Codable {
    
    let id: String
    let availableBalance: String
    let currentBalance: String
    let productName: String
}

struct AccountsResponse: Codable {
    
    let accounts: [AccountResponse]
}

