//
//  GroupedTransactions.swift
//  UBankSwift
//
//  Created by Shane Henderson on 6/5/21.
//

import UIKit

import UIKit

struct GroupedTransactions {
    
    private let order: ComparisonResult
    private let transactions: [Date: [Transaction]]?
    
    init(transactions: [Transaction], ordered by: ComparisonResult) {
        self.order = by
        self.transactions = transactions.grouped(by: \.date)
    }
    
    var dates: [Date]? {
        get {
            return transactions?.keys.sorted(by: { $0.compare($1) == order})
        }
    }
    
    var first: Transaction? {
        get {
            let indexPath = IndexPath(item: 0, section: 0)
            return transaction(at: indexPath)
        }
    }

    var last: Transaction? {
        get {
            guard let date = dates?.last else {
                return nil
            }

            return transactions?[date]?.last
        }
    }
    
    func date(at index: Int) -> Date? {
        return dates?[index]
    }
    
    func index(of date: Date) -> Int? {
        return dates?.firstIndex(of: date)
    }
    
    func numberOfItems(in section: Int) -> Int? {
        guard let date = self.date(at: section) else { return nil }
        
        return transactions?[date]?.count
    }
    
    func transaction(at indexPath: IndexPath) -> Transaction? {
        guard let date = self.date(at: indexPath.section) else { return nil }
        
        return transactions?[date]?[indexPath.item]
    }
    
    func transactions(at date: Date) -> [Transaction]? {
        return transactions?[date]
    }
}

