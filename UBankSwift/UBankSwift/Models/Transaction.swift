//
//  Transaction.swift
//  UBankSwift
//
//  Created by Shane Henderson on 6/5/21.
//

import UIKit

struct Transaction: Datable {
    
    // MARK: - PROPERTIES
    
    let amount: Double
    let date: Date
    let description: String
    let processingStatus: String?
    let runningBalance: Double?
    
    
    // MARK: - INITIALISATION
    
    init?(_ response: TransactionResponse) {
        guard let amount = Double(response.amount) else {
            return nil
        }
        
        self.amount = amount
        date = response.date
        description = response.description
        processingStatus = response.processingStatus
        
        if let balance = response.runningBalance {
            guard let runningBalance = Double(balance) else {
                return nil
            }
            
            self.runningBalance = runningBalance
        }
        else {
            runningBalance = nil
        }
    }
}


// MARK: - HASHABLE

extension Transaction: Hashable {
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(date)
        hasher.combine(amount)
        hasher.combine(description)
    }

    static func == (lhs: Transaction, rhs: Transaction) -> Bool {
        lhs.date == rhs.date && lhs.amount == rhs.amount && lhs.description == rhs.description
    }
}

