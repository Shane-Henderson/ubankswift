//
//  TransactionResponse.swift
//  UBankSwift
//
//  Created by Shane Henderson on 6/5/21.
//

import UIKit

struct TransactionResponse: Codable {
    
    let amount: String
    let date: Date
    let description: String
    let processingStatus: String?
    let runningBalance: String?
}

struct TransactionsResponse: Codable {
    
    let transactions: [TransactionResponse]
}
