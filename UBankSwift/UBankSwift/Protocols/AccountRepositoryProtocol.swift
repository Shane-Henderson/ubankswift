//
//  AccountRepositoryProtocol.swift
//  UBankSwift
//
//  Created by Shane Henderson on 6/5/21.
//

import UIKit

typealias AccountsCompletion = (AccountsResponse?, Error?) -> Void


protocol AccountRepositoryProtocol {
    
    func accounts(completionHandler: @escaping AccountsCompletion)
    
}
