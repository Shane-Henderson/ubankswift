//
//  Datable.swift
//  UBankSwift
//
//  Created by Shane Henderson on 6/5/21.
//

import UIKit

protocol Datable {

    var date: Date { get }

}

