//
//  TransactionRepositoryProtocol.swift
//  UBankSwift
//
//  Created by Shane Henderson on 6/5/21.
//

import UIKit

typealias TransactionsCompletion = (TransactionsResponse?, Error?) -> Void


protocol TransactionRepositoryProtocol {
    
    func transactions(accountId: String, completionHandler: @escaping TransactionsCompletion)
    
}
