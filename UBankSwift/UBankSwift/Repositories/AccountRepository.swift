//
//  AccountRespository.swift
//  UBankSwift
//
//  Created by Shane Henderson on 6/5/21.
//

import UIKit

class AccountRepository: Repository, AccountRepositoryProtocol {

    
    // MARK: - FUNCTIONS
    
    public func accounts(completionHandler: @escaping (AccountsResponse?, Error?) -> Void) {
        let url = URL(string: baseUrl + "accounts.json")!
        
        super.get(url: url, type: AccountsResponse.self) { (response, error) in
            completionHandler(response, error)
        }
    }
}
