//
//  Repository.swift
//  UBankSwift
//
//  Created by Shane Henderson on 6/5/21.
//

import UIKit

class Repository {
    
    // MARK: - PROPERTIES
    
    internal let session = URLSession.shared
    internal let baseUrl = "https://www.ubank.com.au/content/dam/ubank/mobile/coding/"
    
    
    // MARK: - FUNCTIONS
    
    func get<T:Codable>(url: URL, type: T.Type, completionHandler: @escaping (T?, Error?) -> Void) {
        
        let task = session.dataTask(with: url) { (data, response, error) in
            
            let completion: (T?, Error?) -> Void = { (response, error) in
                DispatchQueue.main.async {
                    completionHandler(response, error)
                }
            }
            
            if let error = error {
                completion(nil, error)
                return
            }
            
            guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
                completion(nil, UBankError.serverError)
                return
            }
            
            guard let data = data else {
                completion(nil, UBankError.serverError)
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .formatted(.apiFormatter)
                
                let response = try decoder.decode(T.self, from: data)
                completion(response, nil)
            }
            catch {
                completion(nil, error)
            }
            
        }
        
        task.resume()
    }
    
}
