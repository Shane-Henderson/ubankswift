//
//  TransactionRepository.swift
//  UBankSwift
//
//  Created by Shane Henderson on 6/5/21.
//

import UIKit


class TransactionRepository: Repository, TransactionRepositoryProtocol {

    // MARK: - FUNCTIONS
    
    public func transactions(accountId: String, completionHandler: @escaping (TransactionsResponse?, Error?) -> Void) {
        let url = URL(string: baseUrl + "transactions_\(accountId).json")!
        
        super.get(url: url, type: TransactionsResponse.self) { (response, error) in
            completionHandler(response, error)
        }
    }
}
