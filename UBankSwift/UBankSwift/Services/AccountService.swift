//
//  AccountService.swift
//  UBankSwift
//
//  Created by Shane Henderson on 6/5/21.
//

import UIKit

struct AccountService {
    
    // MARK: - VARIABLES
    
    private let repo: AccountRepositoryProtocol
    
    
    // MARK: - INITIALISATION
    
    init(repository: AccountRepositoryProtocol = AccountRepository()) {
        repo = repository
    }
    
    
    //MARK: - METHODS
    
    public func accounts(completionHandler: @escaping ([Account]?, Error?) -> Void) {
        repo.accounts { (response, error) in
            guard let response = response else {
                completionHandler(nil, error)
                return
            }
            
            let accounts = response.accounts.compactMap { Account($0) }
            completionHandler(accounts, nil)
        }
    }
}
