//
//  TransactionService.swift
//  UBankSwift
//
//  Created by Shane Henderson on 6/5/21.
//

import UIKit

struct TransactionService {
    
    // MARK: - PROPERTIES
    
    private let repo: TransactionRepositoryProtocol
    
    
    // MARK: - INITIALISATION
    
    init(repository: TransactionRepositoryProtocol = TransactionRepository()) {
        repo = repository
    }
    
    
    // MARK: - FUNCTIONS
    
    public func transactions(accountId: String, completionHandler: @escaping ([Transaction]?, Error?) -> Void) {
        repo.transactions(accountId: accountId) { (response, error) in
            guard let response = response else {
                completionHandler(nil, error)
                return
            }
            
            let transactions = response.transactions.compactMap { Transaction($0) }
            completionHandler(transactions, nil)
        }
    }
}
