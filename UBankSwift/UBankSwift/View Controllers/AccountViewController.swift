//
//  AccountViewController.swift
//  UBankSwift
//
//  Created by Shane Henderson on 6/5/21.
//

import UIKit

class AccountViewController: UIViewController {
    
    // MARK: - IBOUTLETS
    
    @IBOutlet private weak var accountView: AccountView!
    @IBOutlet private weak var activityView: UIActivityIndicatorView!
    @IBOutlet private weak var collectionView: UICollectionView!
    
    
    // MARK: - PROPERTIES
    
    private enum ListItem: Hashable {
        case date(Date, Int)
        case transaction(Transaction)
    }
    
    private var dataSource: UICollectionViewDiffableDataSource<Date, ListItem>!
    private var transactions: GroupedTransactions? {
        didSet {
            if self.isViewLoaded {
                self.activityView.stopAnimating()
                configureCollectionViewLayout()
                configureCollectionViewDataSource()
                configureCollectionViewSnapShots()
            }
        }
    }
    
    
    var account: Account! {
        didSet {
            populateTransactions()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        accountView.account = account
        collectionView.contentInset = UIEdgeInsets(top: 200, left: 0, bottom: 0, right: 0)

        if(transactions != nil) {
            self.activityView.stopAnimating()
            configureCollectionViewLayout()
            configureCollectionViewDataSource()
            configureCollectionViewSnapShots()
        }
    }
    
    private func populateTransactions() {
        let service = TransactionService()
        service.transactions(accountId: account.id) { [weak self] (transactions, error) in
            
            guard let transactions = transactions else {
                self?.showError()
                return
            }
            
            self?.transactions = GroupedTransactions(transactions: transactions, ordered: .orderedDescending)
        }
    }
    
    private func showError() {
        activityView.stopAnimating()

        let alert = UIAlertController(title: "An error has occurred",
                                      message: "We could not load your accounts at the moment. Please try again.",
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        
        present(alert, animated: true)
    }
}

extension AccountViewController {
    
    private func configureCollectionViewDataSource() {
        dataSource = UICollectionViewDiffableDataSource<Date, ListItem>(collectionView: collectionView) {
            (collectionView, indexPath, listItem) -> UICollectionViewCell? in
            
            switch listItem {
            case .date(let date, let count):
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Storyboard.dateCellIdentifier, for: indexPath) as! DateViewCell
                cell.date = date
                cell.transactionCount = count
                
                let headerDisclosureOption = UICellAccessory.OutlineDisclosureOptions(style: .header)
                cell.accessories = [.outlineDisclosure(options:headerDisclosureOption)]
                
                return cell
                
            case .transaction(let transaction):
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Storyboard.transactionCellIdentifier, for: indexPath) as! TransactionViewCell
                cell.transaction = transaction
                
                return cell
            }
        }
    }
    
    private func configureCollectionViewSnapShots() {
        var dataSourceSnapshot = NSDiffableDataSourceSnapshot<Date, ListItem>()
        dataSourceSnapshot.appendSections(transactions!.dates!)
        dataSource.apply(dataSourceSnapshot)
        
        for date in transactions!.dates! {
            var sectionSnapshot = NSDiffableDataSourceSectionSnapshot<ListItem>()
            
            let index = self.transactions!.index(of: date)!
            let transactionCount = self.transactions!.numberOfItems(in: index)!
            
            let dateHeader = ListItem.date(date, transactionCount)
            sectionSnapshot.append([dateHeader])
            
            let transactions = self.transactions!.transactions(at: date)!
            let transactionRows = transactions.map { ListItem.transaction($0) }
            sectionSnapshot.append(transactionRows, to: dateHeader)
            sectionSnapshot.expand([dateHeader])
            
            dataSource.apply(sectionSnapshot, to: date, animatingDifferences: false)
        }
    }
    
    private func configureCollectionViewLayout() {
        var layoutConfig = UICollectionLayoutListConfiguration(appearance: .plain)
        layoutConfig.backgroundColor = UIColor.clear
        layoutConfig.headerMode = .firstItemInSection
        let listLayout = UICollectionViewCompositionalLayout.list(using: layoutConfig)
        collectionView.setCollectionViewLayout(listLayout, animated: false)
    }
}
