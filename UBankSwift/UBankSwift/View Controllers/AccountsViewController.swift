//
//  AccountsViewController.swift
//  UBankSwift
//
//  Created by Shane Henderson on 6/5/21.
//

import UIKit

class AccountsViewController: UIPageViewController {

    // MARK: - PROPERTIES
    
    private var controllers: [AccountViewController] = []
    
    
    // MARK: - LIFECYCLE
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        populateViewControllers()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
    }
    
    
    // MARK: - FUNCTIONS
    
    private func indexOf(viewController: AccountViewController) -> Int? {
        return controllers.firstIndex(of: viewController)
    }
    
    private func populateViewControllers () {
        let service = AccountService()
        service.accounts { [weak self] (accounts, error) in
            self?.controllers = []
            
            guard let accounts = accounts else {
                self?.showError()
                return
            }
            
            for account in accounts {
                self?.controllers.append(Storyboard.accountViewController(account))
            }
            
            self?.setViewControllers([self!.controllers.first!], direction: .forward, animated: true)
        }
    }
    
    private func showError() {
        let alert = UIAlertController(title: "An error has occurred",
                                      message: "We could not load your accounts at the moment. Please try again.",
                                      preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        
        present(alert, animated: true)
    }
    
    private func viewController(at index: Int) -> AccountViewController {
        return controllers[index]
    }

}

// MARK: - PAGECONTROLLER DATASOURCE

extension AccountsViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let index = indexOf(viewController: viewController as! AccountViewController), index > 0 else {
            return nil
        }
        
        return self.viewController(at: index - 1)
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let index = indexOf(viewController: viewController as! AccountViewController), index < controllers.count - 1 else {
            return nil
        }
        
        return self.viewController(at: index + 1)
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return controllers.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
}
