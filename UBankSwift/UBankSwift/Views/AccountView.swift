//
//  AccountView.swift
//  UBankSwift
//
//  Created by Shane Henderson on 6/5/21.
//

import UIKit

class AccountView: UIView {

    // MARK: - IBOUTLETS
    
    @IBOutlet private var accountLabel: UILabel!
    @IBOutlet private var balanceLabel: UILabel!
    @IBOutlet private var remainingBalanceLabel: UILabel!

    
    // MARK: - PROPERTIES
    
    let formatter = NumberFormatter.currencyFormatter

    var account: Account! {
        didSet {
            accountLabel.text = account.productName.uppercased()
            balanceLabel.text = formatter.string(from: NSNumber(value: account.currentBalance))
            remainingBalanceLabel.text = formatter.string(from: NSNumber(value: account.availableBalance))
        }
    }
}
