//
//  DateViewCell.swift
//  UBankSwift
//
//  Created by Shane Henderson on 6/5/21.
//

import UIKit

class DateViewCell: UICollectionViewListCell {

    // MARK: - IBOUTLETS
    
    @IBOutlet private var container: UIView!
    @IBOutlet private var dateLabel: UILabel!
    @IBOutlet private var transactionLabel: UILabel!

    
    // MARK: - PROPERTIES
    
    private let formatter = DateFormatter.displayFormatter
    
    var date: Date! {
        didSet {
            
            dateLabel.text = formatter.string(from: date)
        }
    }
    
    var transactionCount: Int! {
        didSet {
            transactionLabel.text = "\(transactionCount!) transaction\(transactionCount > 1 ? "s" : "")"
        }
    }
}
