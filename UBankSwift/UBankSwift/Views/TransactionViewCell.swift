//
//  TransactionViewCell.swift
//  UBankSwift
//
//  Created by Shane Henderson on 6/5/21.
//

import UIKit

class TransactionViewCell: UICollectionViewListCell {

    // MARK: - IBOUTLETS
    
    @IBOutlet private var container: UIView!
    @IBOutlet private var amountLabel: UILabel!
    @IBOutlet private var descriptionLabel: UILabel!
    @IBOutlet private var processingLabel: UILabel!
    @IBOutlet private var runningBalanceLabel: UILabel!

    
    // MARK: - PROPERTIES
    
    private let formatter = NumberFormatter.currencyFormatter
    
    var transaction: Transaction! {
        didSet {
            descriptionLabel.text = transaction.description
            amountLabel.text = formatter.string(from: NSNumber(value: transaction.amount))
            processingLabel.text = transaction.processingStatus?.uppercased() ?? ""
            
            runningBalanceLabel.text = ""
            
            if let runningBalance = transaction.runningBalance {
                runningBalanceLabel.text = formatter.string(from: NSNumber(value: runningBalance))
            }
        }
    }
}
