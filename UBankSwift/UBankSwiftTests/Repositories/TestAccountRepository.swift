//
//  TestAccountRepository.swift
//  UBankSwiftTests
//
//  Created by Shane Henderson on 6/5/21.
//

import UIKit

struct TestAccountRepository: AccountRepositoryProtocol {

    // MARK: - FUNCTIONS
    
    public func accounts(completionHandler: AccountsCompletion) {
        guard let asset = NSDataAsset(name: "accounts") else {
            completionHandler(nil, UBankError.dataError)
            return
        }

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(.apiFormatter)
        
        guard let response = try? decoder.decode(AccountsResponse.self, from: asset.data) else {
            completionHandler(nil, UBankError.decodingError)
            return
        }
        
        completionHandler(response, nil)
    }
}
