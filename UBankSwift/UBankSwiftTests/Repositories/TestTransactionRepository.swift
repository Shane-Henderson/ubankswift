//
//  TestTransactionRepository.swift
//  UBankSwiftTests
//
//  Created by Shane Henderson on 6/5/21.
//

import UIKit

struct TestTransactionRepository: TransactionRepositoryProtocol {

    // MARK: - FUNCTIONS
    
    public func transactions(accountId: String, completionHandler: TransactionsCompletion) {
        guard let asset = NSDataAsset(name: "transactions_\(accountId)") else {
            completionHandler(nil, UBankError.dataError)
            return
        }

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(.apiFormatter)
        
        guard let response = try? decoder.decode(TransactionsResponse.self, from: asset.data) else {
            completionHandler(nil, UBankError.decodingError)
            return
        }
        
        completionHandler(response, nil)
    }
}
