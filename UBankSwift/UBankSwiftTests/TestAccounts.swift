//
//  TestAccounts.swift
//  UBankSwiftTests
//
//  Created by Shane Henderson on 6/5/21.
//

import XCTest
@testable import UBankSwift

class TestAccounts: XCTestCase {

    // MARK: - PROPERTIES
    
    private var accounts: [Account]?
    private var testData: [[String: String]]?
    private var service: AccountService!
    
    
    // MARK: TESTS
    
    override func setUpWithError() throws {
        service = AccountService(repository: TestAccountRepository())
        
        if let data = dictionary(for: "accounts") {
            testData = data["accounts"]
        }
        
        service.accounts { [weak self] (accounts, error) in
            self?.accounts = accounts
        }
    }

    override func tearDownWithError() throws {
        accounts = nil
        testData = nil
    }

    func testTestData() throws {
        XCTAssertNotNil(testData)
    }

    func testTestDataParsed() throws {
        XCTAssertEqual(testData!.count, 3)
    }
    
    func testAccountsParsed() throws {
        XCTAssertNotNil(accounts)
    }

    func testAccountsLength() throws {
        XCTAssertEqual(testData!.count, accounts?.count)
    }
    
    func testAccountIdParsed() throws {
        let testAccount = testData!.first!
        let account = accounts!.first(where: { $0.id == testAccount["id"]! })
        
        XCTAssertNotNil(account)
    }
    
    func testAccountAvailableBalanceParsed() throws {
        let testAccount = testData!.first!
        let account = accounts!.first(where: { $0.id == testAccount["id"] })!
        
        XCTAssertEqual(account.availableBalance, Double(testAccount["availableBalance"]!))
    }

    func testAccountCurrentBalanceParsed() throws {
        let testAccount = testData!.first!
        let account = accounts!.first(where: { $0.id == testAccount["id"] })!
        
        XCTAssertEqual(account.currentBalance, Double(testAccount["currentBalance"]!))
    }

    func testAccountProductNameParsed() throws {
        let testAccount = testData!.first!
        let account = accounts!.first(where: { $0.id == testAccount["id"] })!
        
        XCTAssertEqual(account.productName, testAccount["productName"]!)
    }
    
    
    // MARK: - PRIVATE METHODS
    
    private func dictionary(for file: String) -> [String: [[String: String]]]? {
        guard let asset = NSDataAsset(name: file) else {
            fatalError("Could not retrieve Accounts")
        }

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(.apiFormatter)
        
        return try? decoder.decode([String: [[String: String]]].self, from: asset.data)
    }

}
