//
//  TestGroupedTransactions.swift
//  UBankSwiftTests
//
//  Created by Shane Henderson on 6/5/21.
//

import XCTest
@testable import UBankSwift

class TestGroupedTransactions: XCTestCase {

    // MARK: - PROPERTIES
    
    private let accountId = "12345"
    private var transactions: GroupedTransactions?
    private var testData: [[String: String]]?
    private var service: TransactionService!
    
    override func setUpWithError() throws {
        service = TransactionService(repository: TestTransactionRepository())
        
        if let data = dictionary(for: "transactions_12345") {
            testData = data["transactions"]
        }
        service.transactions(accountId: accountId) { [weak self] (transactions, error) in
            if let transactions = transactions {
                self?.transactions = GroupedTransactions(transactions: transactions, ordered: .orderedDescending)
            }
        }
    }

    
    // MARK: - TESTS
    
    override func tearDownWithError() throws {
        testData = nil
        transactions = nil
    }

    func testTestData() throws {
        XCTAssertNotNil(testData)
    }

    func testTestDataParsed() throws {
        XCTAssertEqual(testData!.count, 38)
    }
    
    func testTransactionsParsed() throws {
        XCTAssertNotNil(transactions)
    }

    func testTransactionsGroupLength() throws {
        let formatter = DateFormatter.apiFormatter
        let dates = testData!.compactMap { formatter.date(from: $0["date"]!) }
        var uniqueDates: [Date] = []
        
        for date in dates {
            if !uniqueDates.contains(where: { Calendar.current.isDate($0, inSameDayAs: date) }) {
                uniqueDates.append(date)
            }
        }
        
        XCTAssertEqual(transactions!.dates!.count, uniqueDates.count)
    }
    
    func testTransactionOrdered() throws {
        let formatter = DateFormatter.apiFormatter
        let dates = testData!.compactMap { formatter.date(from: $0["date"]!) }
        let uniqueDates = Set(dates).sorted(by: { $0.compare($1) == .orderedDescending})
        
        print(uniqueDates)
        
        XCTAssertEqual(uniqueDates.first!, transactions!.first!.date)
        XCTAssertEqual(uniqueDates.last!, transactions!.last!.date)
    }


    private func dictionary(for file: String) -> [String: [[String: String]]]? {
        guard let asset = NSDataAsset(name: file) else {
            fatalError("Could not retrieve Transactions")
        }

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(.apiFormatter)
        
        return try? decoder.decode([String: [[String: String]]].self, from: asset.data)
    }

}
