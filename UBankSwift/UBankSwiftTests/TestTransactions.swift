//
//  TestTransactions.swift
//  UBankSwiftTests
//
//  Created by Shane Henderson on 6/5/21.
//

import XCTest
@testable import UBankSwift


class TestTransactions: XCTestCase {

    // MARK: - PROPERTIES
    
    private let accountId = "12345"
    private var transactions: [Transaction]?
    private var testData: [[String: String]]?
    private var service: TransactionService!
    
    
    // MARK: - TESTS
    
    override func setUpWithError() throws {
        service = TransactionService(repository: TestTransactionRepository())
        
        if let data = dictionary(for: "transactions_12345") {
            testData = data["transactions"]
        }
        
        service.transactions(accountId: accountId) { [weak self] (transactions, error) in
            self?.transactions = transactions
        }
    }

    override func tearDownWithError() throws {
        testData = nil
        transactions = nil
    }

    func testTestData() throws {
        XCTAssertNotNil(testData)
    }

    func testTestDataParsed() throws {
        XCTAssertEqual(testData!.count, 38)
    }
    
    func testTransactionsParsed() throws {
        XCTAssertNotNil(transactions)
    }

    func testTransactionsLength() throws {
        XCTAssertEqual(testData!.count, transactions?.count)
    }
    
    func testTransactionAmountParsed() throws {
        let testTransaction = testData!.first!
        let transaction = transactions!.first!
        
        XCTAssertEqual(transaction.amount, Double(testTransaction["amount"]!))
    }

    func testDateParsed() throws {
        let testTransaction = testData!.first!
        let transaction = transactions!.first!
        
        XCTAssertEqual(transaction.date, DateFormatter.apiFormatter.date(from: testTransaction["date"]!))
    }

    func testTransactionDescriptionParsed() throws {
        let testTransaction = testData!.first!
        let transaction = transactions!.first!
        
        XCTAssertEqual(transaction.description, testTransaction["description"]!)
    }

    func testTransactionProcessingStatus() throws {
        let testTransaction = testData!.first!
        let transaction = transactions!.first!
        
        XCTAssertEqual(transaction.processingStatus, testTransaction["processingStatus"]!)
    }

    func testTransactionRunningBalanceParsed() throws {
        let testTransaction = testData!.first!
        let transaction = transactions!.first!
        
        XCTAssertEqual(transaction.runningBalance, Double(testTransaction["runningBalance"]!))
    }

    
    // MARK: - PRIVATE METHODS
    
    private func dictionary(for file: String) -> [String: [[String: String]]]? {
        guard let asset = NSDataAsset(name: file) else {
            fatalError("Could not retrieve Transactions")
        }

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(.apiFormatter)
        
        return try? decoder.decode([String: [[String: String]]].self, from: asset.data)
    }

}
